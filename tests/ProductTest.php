<?php

use PHPUnit\Framework\TestCase;
use Clippings\Calculator\Product;

class ProductTest extends PHPUnit\Framework\TestCase
{

    public function testProductPrice()
    {
        $item = new Product('Mouse', 129.99);
        $this->assertIsFloat($item->getPrice());
        $this->assertEquals(129.99, $item->getPrice());
    }

}