<?php

use PHPUnit\Framework\TestCase;
use Clippings\Calculator\Product;
use Clippings\Calculator\ProductsBundle;
use Clippings\Calculator\TotalPriceCalculator;

class TotalPriceCalculatorTest extends PHPUnit\Framework\TestCase
{

    public function testGetTotalPriceOfAProduct()
    {
        $mouse = new Product('Mouse', 129.99);
        $calculator = new TotalPriceCalculator([$mouse]);
        $this->assertIsFloat($calculator->getTotal());
        $this->assertEquals(129.99, $calculator->getTotal());
    }

    public function testGetTotalPriceOfABundle()
    {
        $mouse = new Product('Mouse', 129.99);
        $keyboard = new Product('Keyboard', 54.56);
        $bundle = new ProductsBundle([$mouse, $keyboard]);
        $calculator = new TotalPriceCalculator([$bundle]);
        $this->assertIsFloat($calculator->getTotal());
        $this->assertEquals(184.55, $calculator->getTotal());
    }

}