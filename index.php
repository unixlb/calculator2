<?php

include('vendor/autoload.php');

use Clippings\Calculator\Product;
use Clippings\Calculator\ProductsBundle;
use Clippings\Calculator\TotalPriceCalculator;

$mouse = new Product('Mouse', 129.99);
$keyboard = new Product('Keyboard', 59.99);
$bundle = new ProductsBundle([$mouse, $keyboard]);
$calculator = new TotalPriceCalculator([$mouse, $bundle]);

var_dump($calculator->getTotal());