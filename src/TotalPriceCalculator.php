<?php

namespace Clippings\Calculator;

class TotalPriceCalculator
{

    protected $products = [];

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    protected function getProducts() : array
    {
        return $this->products;
    }

    public function getTotal() : float
    {
        $price = [];
        foreach ($this->getProducts() as $product) {
            $price[] = $product->getPrice();
        }
        return array_sum($price);
    }

}