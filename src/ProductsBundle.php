<?php

namespace Clippings\Calculator;

use Clippings\Calculator\ProductInterface;

class ProductsBundle implements ProductInterface
{

    protected $products = [];

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function getPrice() : float
    {
        $price = [];
        foreach ($this->getProducts() as $product) {
            $price[] = $product->getPrice();
        }
        return array_sum($price);
    }

    protected function getProducts() 
    {
        return $this->products;
    }

}