<?php

namespace Clippings\Calculator;

interface ProductInterface
{

    public function getPrice() : float;

}