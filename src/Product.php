<?php

namespace Clippings\Calculator;

use Clippings\Calculator\ProductInterface;

class Product implements ProductInterface
{

    protected $name = null;
    protected $price = null;

    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function __toString() : string
    {
        return $this->name.' '.$this->price;
    }

}